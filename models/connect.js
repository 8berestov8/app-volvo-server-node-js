const dbConn = require("../config/db");
const mongoose = require("mongoose");
/**
 * Подключение к базе данных
 */
module.exports = function connDB(req, res, cd) {
  mongoose
    .connect(dbConn.url, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false,
    })
    .then(() => console.log("DB connected"))
    .catch(() => console.log("DB Connection failed"));
  cd(dbConn);
};
